"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Commentator = void 0;
const data_1 = require("../../data");
const helpers_1 = require("../helpers");
class Commentator {
    constructor(io, roomName, rooms) {
        this.io = io;
        this.ioEmit = helpers_1.ioEmitters(io);
        this.roomName = roomName;
        this.rooms = rooms;
        this.racers = rooms
            .getRoom(this.roomName)
            .users.reduce((acc, user) => {
            acc[user.name] = data_1.commentator.cars[Math.floor(Math.random() * data_1.commentator.cars.length)];
            return acc;
        }, {});
        this.mode = 'countdown';
        this.time = 0;
        this.racerTime = {};
    }
    onTick(timeLeft) {
        switch (this.mode) {
            case 'countdown':
                this.onCountdown(timeLeft);
                break;
            case 'race':
                this.onRace(timeLeft);
                break;
        }
        this.time = 60 - timeLeft;
    }
    onCountdown(timeLeft) {
        switch (timeLeft) {
            case 10:
                this.intro();
                break;
            case 5:
                this.announceRacers();
                break;
            case 0:
                this.ioEmit.toRoom(this.roomName)('COMMENTATOR')('Погнали!');
                break;
        }
    }
    intro() {
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(data_1.commentator.intro);
    }
    announceRacers() {
        const racerPhrases = Object.entries(this.racers)
            .map(([name, car], index) => `${name} на ${car} под номером ${index + 1}`)
            .join(', ');
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(data_1.commentator.announcements + racerPhrases);
    }
    onRace(timeLeft) {
        switch (timeLeft) {
            case 30:
                this.status();
        }
    }
    status() {
        const racers = this.rooms
            .getRoom(this.roomName)
            .users.sort((user_a, user_b) => user_b.progress - user_a.progress)
            .slice(0, 3);
        const phrases = racers === null || racers === void 0 ? void 0 : racers.map((racer, index) => {
            switch (index) {
                case 0:
                    return `${racer.name} на ${this.racers[racer.name]} сейчас первый`;
                case 1:
                    return `за ним идет ${racer.name}`;
                case 2:
                    return `а третьим идет ${racer.name}...`;
            }
        }).join(', ');
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(phrases);
    }
    onFinalStraight() {
        var _a;
        const racers = (_a = this.rooms
            .getRoom(this.roomName)) === null || _a === void 0 ? void 0 : _a.users.sort((user_a, user_b) => user_b.progress - user_a.progress).slice(0, 3);
        const phrases = racers
            .map((racer, index) => {
            switch (index) {
                case 0:
                    return `До финиша осталось совсем немного и похоже что первым его может пересечь ${racer.name} на своем ${this.racers[racer.name]}.`;
                case 1:
                    return ` Второе место может достаться ${racer.name}`;
                case 2:
                    return `или ${racer.name}`;
            }
        })
            .join(' ') + '. Но давайте дождемся финиша.';
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(phrases);
    }
    finishes(name) {
        this.racerTime[name] = this.time;
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(`Финишную прямую пересекает ${name}`);
    }
    raceEnds(name) {
        var _a;
        const racers = (_a = this.rooms
            .getRoom(this.roomName)) === null || _a === void 0 ? void 0 : _a.leaderBoard.filter((racer) => racer.progress === 100);
        const phrases = racers === null || racers === void 0 ? void 0 : racers.map((racer, index) => {
            switch (index) {
                case 0:
                    return `первое место занимает ${racer.name} за рекордные ${this.racerTime[racer.name]} секунд(ы)`;
                case 1:
                    return `второе место - ${racer.name} за ${this.racerTime[racer.name]} секунд(ы)`;
                case 2:
                    return `третье -  ${racer.name} со всемнем ${this.racerTime[racer.name]} секунд(ы)`;
            }
        }).join(', ');
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(name
            ? `Финишную прямую пересекает ${name} и финальный результат: ${phrases}`
            : `Время вышло и финальный результат: ${phrases}`);
    }
    switchMode() {
        this.mode = this.mode === 'countdown' ? 'race' : 'countdown';
    }
}
exports.Commentator = Commentator;
