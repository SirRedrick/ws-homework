"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Room = void 0;
const config = __importStar(require("../config"));
class Room {
    constructor(name) {
        this.name = name;
        this.users = [];
        this.leaderBoard = [];
        this.isRunning = false;
    }
    update({ name, users, leaderBoard, isRunning }) {
        this.name = name;
        this.users = users;
        this.leaderBoard = leaderBoard;
        this.isRunning = isRunning;
    }
    reset() {
        this.users = this.users.map(user => (Object.assign(Object.assign({}, user), { isReady: false, progress: 0 })));
        this.isRunning = false;
        this.leaderBoard = [];
    }
    toggleRunning() {
        this.isRunning = !this.isRunning;
    }
    join(username) {
        if (this.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
            throw new Error(`Room is full, maximum amount is ${config.MAXIMUM_USERS_FOR_ONE_ROOM}`);
        }
        this.users.push({
            name: username,
            isReady: false,
            progress: 0
        });
    }
    removeUser(name) {
        this.users = this.users.filter(user => user.name !== name);
        this.leaderBoard = this.leaderBoard.filter(user => user.name !== name);
    }
    toggleReady(username) {
        this.users = this.users.map(user => user.name === username ? Object.assign(Object.assign({}, user), { isReady: !user.isReady }) : user);
        if (this.isEveryoneReady()) {
            this.isRunning = true;
        }
    }
    isEveryoneReady() {
        return this.users.every(user => user.isReady);
    }
    isFinished() {
        return this.users.every(user => user.progress === 100);
    }
    updateProgress(username, progress) {
        this.users = this.users.map(user => (user.name === username ? Object.assign(Object.assign({}, user), { progress }) : user));
        if (progress === 100) {
            this.leaderBoard.push(this.users.find(u => u.name === username));
        }
    }
    addDnfToLeaderBoard() {
        this.leaderBoard = this.leaderBoard.concat(this.getDNF());
    }
    getDNF() {
        return this.users
            .filter(user => user.progress < 100)
            .sort((user_a, user_b) => user_b.progress - user_a.progress);
    }
}
exports.Room = Room;
