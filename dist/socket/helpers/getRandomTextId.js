"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRandomTextId = void 0;
const data_1 = require("../../data");
const getRandomTextId = () => Math.floor(Math.random() * data_1.texts.length);
exports.getRandomTextId = getRandomTextId;
