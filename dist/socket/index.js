"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const config = __importStar(require("./config"));
const rooms_1 = require("./state/rooms");
const timer_1 = require("./state/timer");
const routes_1 = require("./routes");
const helpers_1 = require("./helpers");
exports.default = (io) => {
    const activeUsers = new Set();
    const runningTimers = new Map();
    const commentators = new Map();
    const rooms = new rooms_1.Rooms();
    const emit = helpers_1.ioEmitters(io);
    const tick = (name, commentator) => (time) => {
        const timeLeft = Math.round(time / 1000);
        emit.toRoom(name)('ROOM_TICK')(timeLeft);
        commentator.onTick(timeLeft);
    };
    const endGame = (name, commentator) => () => {
        rooms.addDnfToLeaderBoard(name);
        emit.toRoom(name)('ROOM_GAME_END')(rooms.getLeaderBoard(name));
        commentator.raceEnds();
        rooms.resetRoom(name);
        emit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
        emit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
    };
    const startGame = (name, commentator) => () => {
        emit.toRoom(name)('ROOM_GAME_START')();
        commentator.switchMode();
        const onTick = [tick(name, commentator)];
        const onEnd = [endGame(name, commentator)];
        const timer = new timer_1.Timer(config.SECONDS_FOR_GAME * 1000, onTick, onEnd);
        timer.start();
        runningTimers.set(name, timer);
    };
    const startCountdown = (name) => {
        emit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
        emit.toRoom(name)('ROOM_EVERYONE_READY')(helpers_1.getRandomTextId());
        const commentator = new routes_1.Commentator(io, name, rooms);
        const onTick = [tick(name, commentator)];
        const onEnd = [startGame(name, commentator)];
        const timer = new timer_1.Timer(config.SECONDS_TIMER_BEFORE_START_GAME * 1000, onTick, onEnd);
        timer.start();
        runningTimers.set(name, timer);
    };
    io.on('connection', socket => {
        const username = socket.handshake.query.username;
        routes_1.users(socket, username, activeUsers, rooms);
        routes_1.hub(io, socket, username, rooms, runningTimers, startCountdown);
        routes_1.room(io, socket, username, rooms, runningTimers, commentators, startCountdown);
    });
};
