import { createRoom } from '../helpers/index.mjs';

class Hub {
  constructor(socket) {
    this.socket = socket;
    this.roomsContainer = document.getElementById('rooms-container');

    this.createRoom = this.createRoom.bind(this);
    this.joinRoom = this.joinRoom.bind(this);

    this.roomsContainer.addEventListener('click', this.joinRoom);
    document.getElementById('add-room-btn').addEventListener('click', this.createRoom);

    this.rooms = [];
  }

  updateRooms(rooms) {
    this.rooms = rooms;
    this.renderRooms();
  }

  renderRooms() {
    const roomElements = this.rooms.map(createRoom);

    this.roomsContainer.innerHTML = '';
    this.roomsContainer.append(...roomElements);
  }

  createRoom() {
    const name = prompt('Enter a room name');

    switch (name) {
      case null:
        return;
      case '':
        alert('Room name should not be empty');
        break;
      default:
        this.socket.emit('HUB_CREATE_ROOM', name);
    }
  }

  joinRoom(e) {
    const id = e.target.id;
    this.socket.emit('HUB_JOIN_ROOM', id);
  }
}

export { Hub };
