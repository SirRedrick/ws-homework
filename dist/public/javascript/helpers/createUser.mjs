import { createElement } from './createElement.mjs';

export const createUser = (user, currentUser) => {
  const readyStatus = createElement({
    tagName: 'span',
    className: user.isReady ? 'ready-status-green' : 'ready-status-red'
  });

  const userInfo = createElement({
    tagName: 'div',
    className: 'user-info',
    children: [readyStatus, user.name, user.name === currentUser ? '(you)' : '']
  });
  3;

  const progressBar = createElement({
    tagName: 'div',
    className: `user-progress ${user.name}${user.progress === 100 ? ' complete' : ''}`,
    attributes: {
      style: `width: ${user.progress}%`
    }
  });
  const progressBarWrapper = createElement({
    tagName: 'div',
    className: 'progress-bar-wrapper',
    children: [progressBar]
  });

  return createElement({
    tagName: 'div',
    children: [userInfo, progressBarWrapper]
  });
};
