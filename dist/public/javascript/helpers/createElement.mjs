const createElement = ({ tagName, className, attributes, nameSpace, children, eventListeners }) => {
  const element = nameSpace
    ? document.createElementNS(nameSpace, tagName)
    : document.createElement(tagName);

  if (className) {
    addClasses(element, className);
  }

  if (attributes) {
    addAttributes(element, attributes);
  }

  if (children) {
    element.append(...children);
  }

  if (eventListeners) {
    addEventListeners(element, eventListeners);
  }

  return element;
};

const addClasses = (element, className) => {
  const classNames = className.split(' ').filter(Boolean);
  element.classList.add(...classNames);
};

const addAttributes = (element, attributes) => {
  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
};

const addEventListeners = (element, eventListeners) => {
  eventListeners.forEach(([eventType, callback]) => element.addEventListener(eventType, callback));
};

export { createElement };
