"use strict";
// const texts = [
//   'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum incidunt quam mollitia ut praesentium vitae laborum atque quos similique provident!',
//   'Nisi, amet quos! Esse neque eius magni voluptas consequuntur suscipit repellendus, beatae facilis? Qui sequi tempora nostrum mollitia, impedit officia!',
//   'Distinctio nihil est necessitatibus quas eaque perferendis ea maxime. Aut, laborum! Maxime commodi rerum deserunt. Deserunt eaque doloribus similique deleniti?',
//   'Rem obcaecati dolores, quasi impedit tempore dolorem quod similique ratione quos debitis. Commodi tenetur eligendi eaque dolore quam enim illum.',
//   'Eius accusamus aut adipisci sint minus a ad veritatis, animi facilis nesciunt quasi dolores explicabo facere non neque possimus ipsam?',
//   'Molestias possimus est laboriosam esse ab exercitationem ex asperiores, omnis autem saepe beatae ullam incidunt ducimus accusamus nobis eum? Distinctio!',
//   'Rem laborum, aut officia, iusto perspiciatis praesentium facere, dolor natus accusamus fuga beatae unde explicabo reprehenderit delectus animi optio quos.'
// ];
Object.defineProperty(exports, "__esModule", { value: true });
exports.commentator = exports.texts = void 0;
const texts = [
    'text number 1',
    'text number 2',
    'text number 3',
    'text number 4',
    'text number 5',
    'text number 6',
    'text number 7'
];
exports.texts = texts;
const commentator = {
    intro: 'На улице сейчас немного пасмурно, но на Львов Арена сейчас просто замечательная атмосфера: двигатели рычат, зрители улыбаются а гонщики едва заметно нервничают и готовят своих железных коней к заезду. А комментировать всё это действо буду я, Эскейп Энтерович и я рад вас приветствовать со словами Доброго Вам дня, господа!',
    announcements: 'А тем временем, список гонщиков: ',
    cars: [
        'Ferrari',
        'Renault',
        'McLaren',
        'Mercedes',
        'Honda',
        'Toyota',
        'BMW',
        'Porsche',
        'Jaguar',
        'Audi'
    ]
};
exports.commentator = commentator;
