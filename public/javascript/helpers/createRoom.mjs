import { createElement } from './createElement.mjs';

export const createRoom = ({ name, users, isRunning }) => {
  const isHidden = users.length > 4 || isRunning;

  const title = createElement({
    tagName: 'h2',
    className: 'room-name',
    children: [name]
  });

  const joinBtn = createElement({
    tagName: 'button',
    className: 'join-btn btn',
    attributes: {
      id: name
    },
    children: ['Join']
  });

  const userCounter = createElement({
    tagName: 'div',
    className: 'user-number',
    children: [`${users.length} users connected`]
  });

  return createElement({
    tagName: 'div',
    className: `room ${isHidden ? 'display-none' : 'flex'}`,
    children: [title, joinBtn, userCounter]
  });
};
