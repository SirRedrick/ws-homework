import { User } from './user';

type Room = {
  name: string;
  users: User[];
  isRunning: boolean;
  leaderBoard: User[];
};

export { Room };
