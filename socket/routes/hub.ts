import { Server, Socket } from 'socket.io';
import { getCurrentRoom, ioEmitters, socketEmitters } from '../helpers';
import { Timer } from '../state/timer';
import { Rooms } from '../state/rooms';

const hub = (
  io: Server,
  socket: Socket,
  username: string,
  rooms: Rooms,
  runningTimers: Map<string, Timer>,
  startCountdown: (name: string) => void
) => {
  const ioEmit = ioEmitters(io);
  const socketEmit = socketEmitters(socket);

  const leaveRoom = () => {
    const name = getCurrentRoom(socket);
    let room = null;

    try {
      room = rooms.getRoom(name);
    } catch (err) {
      return socketEmit.toSocket('ROOM_ERR')(err.message);
    }

    if (room.users.length > 1) {
      rooms.removeUser(name, username);

      if (rooms.isRoomReady(name) && !rooms.getRoom(name).isRunning) {
        startCountdown(name);
        rooms.toggleRoomRunning(name);
      }

      if (rooms.isRoomFinished(name)) {
        const timer = runningTimers.get(name);

        if (timer) {
          timer.stop();
          runningTimers.delete(name);
        }
        ioEmit.toRoom(name)('ROOM_GAME_END')(rooms.getLeaderBoard(name));
      }

      socketEmit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
    } else {
      const timer = runningTimers.get(name);

      if (timer) {
        timer.stop();
        runningTimers.delete(name);
      }

      rooms.removeRoom(name);
    }

    socket.leave(name);
    ioEmit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
  };

  socket.on('HUB_CREATE_ROOM', name => {
    try {
      rooms.createRoom(name);
    } catch (err) {
      return socketEmit.toSocket('HUB_CREATE_ROOM_FAILURE')(err.message);
    }

    socketEmit.toSocket('HUB_CREATE_ROOM_SUCCESS')(name);
    ioEmit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
  });

  socket.on('HUB_JOIN_ROOM', name => {
    try {
      rooms.joinRoom(name, username);
    } catch (err) {
      return socketEmit.toSocket('ROOM_ERR')(err.message);
    }

    const room = rooms.getRoom(name);

    socket.join(name);
    socketEmit.toRoom(name)('ROOM_UPDATE')(room);
    socketEmit.toSocket('HUB_JOIN_ROOM_SUCCESS')(room);
    ioEmit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
  });

  socket.on('HUB_LEAVE_ROOM', () => {
    leaveRoom();
    socketEmit.toSocket('HUB_LEAVE_ROOM_SUCCESS');
  });

  socket.on('disconnecting', () => leaveRoom());
};

export { hub };
