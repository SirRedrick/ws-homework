import { Socket } from 'socket.io';
import { Rooms } from '../state/rooms';
import { socketEmitters } from '../helpers';

const users = (socket: Socket, username: string, activeUsers: Set<string>, rooms: Rooms) => {
  const socketEmit = socketEmitters(socket);

  if (activeUsers.has(username)) {
    return socketEmit.toSocket('LOGIN_FAILURE')('Specified username has been already taken');
  }

  activeUsers.add(username);
  socketEmit.toSocket('LOGIN_SUCCESS')(rooms.getRooms());

  socket.on('disconnect', () => {
    activeUsers.delete(username);
  });
};

export { users };
