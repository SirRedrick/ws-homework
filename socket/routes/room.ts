import { Server, Socket } from 'socket.io';
import { getCurrentRoom, ioEmitters, socketEmitters } from '../helpers';
import { Rooms } from '../state/rooms';
import { Timer } from '../state/timer';
import { Commentator } from './commentator';

const room = (
  io: Server,
  socket: Socket,
  username: string,
  rooms: Rooms,
  runningTimers: Map<string, Timer>,
  commentators: Map<string, Commentator>,
  startCountdown: (name: string) => void
) => {
  const ioEmit = ioEmitters(io);
  const socketEmit = socketEmitters(socket);

  socket.on('ROOM_TOGGLE_READY', () => {
    const name = getCurrentRoom(socket);
    rooms.toggleReady(name, username);

    if (rooms.isRoomReady(name)) {
      startCountdown(name);
    }

    socketEmit.toSocket('ROOM_TOGGLE_READY_SUCCESS');
    ioEmit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
  });

  socket.on('ROOM_UPDATE_PROGRESS', progress => {
    const name = getCurrentRoom(socket);
    try {
      rooms.updateProgress(name, username, progress);
    } catch (err) {
      socketEmit.toSocket('ROOM_ERROR')(err.message);
    }

    if (progress === 100) {
      commentators.get(name)?.finishes(username);
    }

    if (rooms.isRoomFinished(name)) {
      const timer = runningTimers.get(name);

      if (timer) {
        timer.stop();
        runningTimers.delete(name);
      }
      ioEmit.toRoom(name)('ROOM_GAME_END')(rooms.getLeaderBoard(name));
      commentators.get(name)?.raceEnds(username);

      rooms.resetRoom(name);

      ioEmit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
    }

    ioEmit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
  });

  socket.on('ROOM_THIRTY_SYMBOLS', () => {
    const name = getCurrentRoom(socket);
    commentators.get(name)?.onFinalStraight();
  });
};

export { room };
