import { Socket } from 'socket.io';

const getCurrentRoom = (socket: Socket) => {
  const roomsIter = socket.rooms.values();
  roomsIter.next();
  return roomsIter.next().value;
};

export { getCurrentRoom };
