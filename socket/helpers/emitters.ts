import { Server, Socket } from 'socket.io';
import fp from 'lodash/fp';

const ioEmitters = (io: Server) => {
  return {
    toAll: fp.curry((eventType: string, data: any) => io.emit(eventType, data)),
    toRoom: fp.curry((name: string, eventType: string, data: any) => {
      if (data === undefined) {
        io.to(name).emit(eventType);
      } else {
        io.to(name).emit(eventType, data);
      }
    })
  };
};

const socketEmitters = (socket: Socket) => {
  return {
    toSocket: fp.curry((eventType: string, data: any) => socket.emit(eventType, data)),
    toRoom: fp.curry((name: string, eventType: string, data: any) => {
      if (data === undefined) {
        socket.to(name).emit(eventType);
      } else {
        socket.to(name).emit(eventType, data);
      }
    })
  };
};

export { ioEmitters, socketEmitters };
